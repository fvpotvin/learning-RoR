# Learning RoR

### Structure


`app/controllers` contains code that renders the application. 
It follows REST architecture, with routes followin this pattern:
![Screenshot from 2022-12-14 14-27-56.png](./Screenshot from 2022-12-14 14-27-56.png)

There is some magic happening when scaffolding:
The edit route has a function that is automagically called defined at the bottom in the `private` section

`app/models/` defines the models. Not 100% sure yet how it works, but if it inherits ApplicationRecord, it comes with view functionalities.
    Example usage so far:
    Adding `has_many :microposts` to `user.rb` creates an association that a user can have many microposts.



`app/view/{foler_of_model_name}/index.html.erb` (on GitLab, they seem to be `.haml` files) Files with the HTML to display the page




## Currently at chapter 3:
https://www.railstutorial.org/book/static_pages


